/**
replace code as below in .\YourCordovaProject\platforms\browser\cordova\lib\check_reqs.js
**/

/*module.exports.run = function () {
    return Promise.resolve();
};*/

module.exports.run = function () {
    var requirements = [new Requirement('browser', 'Browser', true, true)];
    return Promise.resolve().then(function(){
        return requirements;
        //or just return [];
    });
};

/*Not need if retun []*/
var Requirement = function (id, name, version, installed) {
    this.id = id;
    this.name = name;
    this.installed = installed || false;
    this.metadata = {
        version: version
    };
};