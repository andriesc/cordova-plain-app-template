#!/usr/bin/env node
module.exports = function(ctx) {
  console.log("---===> starting to fix browser platform bug");

  // only act if browser platform is part of the project
  if (ctx.opts.platforms.indexOf("browser") < 0) {
       console.log("---===> browser platform not affected, exiting normally");
       return;
  }

  //include the fs, path modules
  var fs = require("fs");
  var path = require("path");

  //gets file name and adds it to dir2
  var f = path.basename("scripts/check_reqs.js");
  var source = fs.createReadStream("scripts/check_reqs.js");
  var dest = fs.createWriteStream(path.resolve("platforms/browser/cordova/lib/", f));

  source.pipe(dest);
  source.on("end", function() { console.log("---===> succesfully copied browser platform bugfix"); });
  source.on("error", function(err) { console.log(err); });
};
